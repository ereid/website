<div id="submenu">
	<ul>
		<li><a href="/tro/">Tabletop Roleplay Online</a></li>
		<li><a href="/tro/faq/">FAQ</a></li>
		<li><a href="/tro/download/">Download</a></li>
	</ul>
</div>
<div id="column_l">
	<h2>Tabletop Roleplay Online</h2>
	<p>Welcome to TRO (Tabletop Roleplay Online)! TRO is the original and best way to play Tabletop (or Pen and Paper, if you prefer) RPGs over the internet. What's more - it's free! Always has been, always will be. If you've no idea what's going on, you may wish to check the <a href="/tro/faq/">FAQ</a>; for help.</p>
		<p>The latest release of TRO is always available on the <a 
		href="/tro/download/">download</a> page.</p>
	</div>
</div>
