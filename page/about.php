<div id="column_l">
	<h2>Who is Euan Reid?</h2>
	<p>Just this guy, you know? Okay, presumably if you're looking here you want a little more detail. I'm a DevOps Engineer at Cloudreach, recently graduated from the University of Edinburgh with a Software Engineering degree, and have previously worked in tech fields varying from SEO to mobile app development, stopping firmly along the way at web development.</p>
	<p>In my spare time, my primary hobby is tabletop roleplaying - think Dungeons &amp; Dragons et al - which I indulge on a regular basis at both GEAS (the Edinburgh University Roleplaying Society) and ORC Edinburgh (Open Roleplaying Community). I'm also the coordinator of Conpulsion 2015 - Scotland's oldest, largest gaming convention.</p>
	<p>Otherwise, I enjoy forum roleplaying (which falls somewhere between creative writing and the aforementioned tabletop), video games, and formerly competed at the student national level in archery. Wall climbing and karate are other sportier activities I've previously participated in, but have lapsed of late. I do manage to get to the gym fairly regularly though.</p>
</div>
<div id="column_r">
	&nbsp;<h4>In Short</h4>
	<p>DevOps engineer, web developer, roleplayer, gamer, geek.</p>
</div>
