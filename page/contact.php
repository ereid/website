<div id="column_l">
	<h2>Contact Me</h2>
	<h4>General Details</h4>
	<p>Phone: <span style="unicode-bidi:bidi-override; direction: rtl;">9819-693 (406)</span><br>
	Email (personal): <span style="unicode-bidi:bidi-override; direction: rtl;">moc.diernaue@naue</span><br>
	Email (business): <span style="unicode-bidi:bidi-override; direction: rtl;">moc.hcaerduolc@dier.naue</span></p>
</div>

<div id="column_r">
	<h4>Mailing Address</h4>
	<p>Euan Reid<br>
	Cloudreach Canada
	Suite 400<br>
	123 Cambie Street<br>
	Vancouver<br>
	V6B 1B8</p>
</div>
