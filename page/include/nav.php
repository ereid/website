<body>
<div id="fb-root"></div>
<script>(function(d, id) {
  var js, fjs = d.getElementsByTagName('script')[0];
  if (d.getElementById(id)) return;
  js = d.createElement('script'); js.id = id; js.async = true;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'facebook-jssdk'));</script>

<div id="container-top">
	<div id="masthead">
		<h1>Euan Reid</h1>
	</div>
	<div id="navigation">
		<ul>
			<li><a href="/">Home</a></li>
			<li><a href="/about/">About Me</a></li>
			<li><a href="/projects/">Projects</a>
				<ul>
					<li><a href="/rp/">Roleplaying for Beginners</a></li>
					<li><a href="/tro/">Tabletop Roleplay Online</a></li>
					<li><a href="http://reid.spreadshirt.co.uk" target="_blank">T-Shirt Shop</a></li>
				</ul></li>
			<li><a href="http://static.euanreid.com/files/CV.pdf" target="_blank">CV</a></li>
			<li><a href="/contact/">Contact</a></li>
			<li><a href="/links/">Links</a></li>
		</ul>
		<div id="fb-like" class="fb-like" data-send="false" data-layout="button_count" data-width="90" data-show-faces="false"></div>
	</div>
</div>
<div id="container">
	<div id="main">
