<div id="content">
	<h2>Programs! Websites! Exclamation Marks!</h2>
	<p>This sub-site forms a little hub for various projects I've been working on - most have a basis of coding, but where some are desktop applications, other are web-based, and yet others <em>don't</em> have any code-basedness to them! Feel free to have a poke around and try things out.</p>
	
	<h3>Desktop Apps</h3>
	<ul>
		<li ><a href="/tro/">Tabletop Roleplay Online</a><br>
		A quick and simple way to play tabletop RPGs over the internet</li>
	</ul>

	<h3>Websites</h3>
	<ul>
		<li><a href="/">EuanReid.com</a> (formerly ReidE96.com)<br>
		Ok, it's cheating somewhat to list it, but this entire site is something I've been working on-and-off on for... years now, through various iterations and complete rewrites.</li>
		<li><a href="http://www.phoenix-rp.com">Phoenix Roleplaying</a><br>
		A play-by-post roleplaying forum I maintain the technical side of. Currently planning new code to replace the existing boards.</li>
		<li><a href="http://www.swtorcharacters.com">SWTOR Characters</a><br>
		A little website of <a href="http://www.swtor.com">SWTOR</a> characters I and friends play. Given a fondness for roleplaying, bio pages seemed like a fun idea.</li>
	</ul>

	<h3>Other</h3>
	<ul>
		<li><a href="/rp/">A Beginner's Guide to Roleplaying</a><br>
		Originally posted on <a href="http://www.ownedcore.com">OwnedCore</a> back when it was MMOwned.com, reworked and better presented here. A guide on the basics of text-based roleplaying.</li>
	</ul>
</div>
