<?php
$page = isset($_GET['page']) ? $_GET['page'] : 'home';
if (strpos($page, '/') !== false){
	$parts = explode('/', $page);
	$subsite = $parts[0];
	$page = $parts[1];
	$content = "$subsite/$page.php";
} else {
	$content = "$page.php";
}
$title = ($page == 'home') ? 'A Homepage of Sorts' : ucwords($page);
$title = ($subsite) ? ucwords($subsite) . " - $title" : $title;

// Hacking around
$title = str_replace(array('Tro','Rp'), array('Tabletop Roleplay Online', 'Beginner\'s Guide to RP'), $title);

ob_start();
require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/include/nav.php');
if (file_exists($_SERVER['DOCUMENT_ROOT']."/page/$content")){	
	require_once($_SERVER['DOCUMENT_ROOT']."/page/$content");
} else {
	require_once($_SERVER['DOCUMENT_ROOT'].'/page/404.php');
}
require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.php');
ob_end_flush();
?>
