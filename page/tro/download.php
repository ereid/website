<div id="submenu">
	<ul>
		<li><a href="/tro/">Tabletop Roleplay Online</a></li>
		<li><a href="/tro/faq/">FAQ</a></li>
		<li><a href="/tro/download/">Download</a></li>
	</ul>
</div>

<div id="column_l">
	<h2>Download</h2>
	<p>Here you can download the latest releases of both the TRO client and server.</p>
	
	<h3>Client</h3>
	<ul>
		<li>Release: 1.1</li>
		<li>Released 28th February 2010</li>
		<li><a href="/tro-files/tro.exe">Download</a></li>
	</ul>	

	<h3>Server</h3>
	<ul>
		<li>Release: 1.1</li>
		<li>Released 28th February 2010</li>
		<li><a href="/tro-files/tro-server.exe">Download</a></li>
	</ul>

	<h3>Additional Downloads</h3>
	<p>TRO requires <a href="http://www.microsoft.com/downloads/details.aspx?FamilyID=a9ef9a95-58d2-4e51-a4b7-bea3cc6962cb&displaylang=en">.NET 4</a> to run.</p>
	<p>Perhaps, at some point, you'll get plugins for some kind of character 
	sheet thingy here. We'll see.</p>
	
	<h3>Older Releases</h3>
	<ul>
		<li>1.0 (19th January 2010) - <a href="/tro-files/tro-20100119.exe">Download</a></li>
		<li>Server 1.0 (19th January 2010) - <a href="/tro-files/tro-server-20100119.exe">Download</a></li>
	</ul>
</div>
