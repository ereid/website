<div id="submenu">
	<ul>
		<li><a href="/tro/">Tabletop Roleplay Online</a></li>
		<li><a href="/tro/faq/">FAQ</a></li>
		<li><a href="/tro/download/">Download</a></li>
	</ul>
</div>

<div id="column_l">
	<h2>FAQ</h2>
	<p>Here I will try and answer the most common questions asked about TRO. If you've got a question that isn't listed here, feel free to get in touch via the <a href="/contact/">contact page</a>.</p>
	
	<h3>What is Tabletop Roleplay Online?</h3>
	<p>Tabletop Roleplay Online is exactly what the name implies - a way to play your favourite tabletop RPGs over the internet. There are two parts - the client, which everyone needs to have in order to play, and the server, which one user needs to use to host a game session.</p>
	
	<h3>What does TRO stand for?</h3>
	<p><strong>T</strong>abletop <strong>R</strong>oleplay <strong>O</strong>nline</p>
	
	<h3>What do I need to get started?</h3>
	<p>First off, you'll need a bunch of friends to play with. They can be 	folk you know in real life, or online acquaintances, or a mixture of both. All the players need to get themselves a copy of the TRO client. You can get this from the <a href="/tro/download/">download</a> page.</p>
	<p>You'll also need one player to host the game. Generally the player with the fastest internet connection should fill this role. As well as the client, the host needs the server, which they can again get from the <a href="/tro/download/">download</a> page.</p>
	<p>Lastly, everyone needs to know the host's IP. You can find this in many ways, but the easiest is to go to <a href="http://www.whatismyip.com">What Is My Ip?</a> which displays it at the top of the page. Your IP is the group of numbers and dots - 186.89.122.49 is an example of an IP.</p>
	<p>Once they have it, the host needs to enter their IP into the server and click the "Start Listening" button. Once the server reports that it's "waiting for connections", everyone should open their clients, and enter the host's IP along with their own desired player name in the boxes on the popup, before clicking connect. All being well, the players should all connect, and you're ready to go!</p>
	
	<h3>I'm behind a router. What port(s) should I forward?</h3>
	<p>TRO uses port 1986 for all communications.</p>
	
	<h3>This is great! How can I help out?</h3>
	<p>In terms of code development, not much - TRO is just something I do to amuse myself during my spare time (when I'm not playing games or out	and about).
</div>
