<div id="submenu">
	<ul>
		<li><a href="/rp/">Preface</a></li>
		<li><a href="/rp/prologue/">Prologue/FAQ</a></li>
	</ul>
</div>
<div id="column_l">
	<h2>Prologue/FAQ</h2>
	<h3>Foreword</h3>
<p>I have noticed that some articles on this subject tend to show writer's own point of view as to what role playing is and how it should be done. The problem is that everyone is different, and what is right for one person may not be for another. So I just intend to give some general useful information, without ramming too much of my own opinion down your throat. This is not indented to be a definitive guide - merely a starting point of things to bear in mind. If you read several different articles on role playing and some of the views seem to conflict, remember its just different peoples opinions. Take from it what you think is good, and if you think something does not sound right for you then it may just be their role play style - you don't have to follow it.</p>
<h3>What is role playing?</h3>
<p>Role playing is a bit like acting, in that you put yourself in someone elses shoes. You get to go on a holiday from yourself and do things that you would not normally do or say. In short; you take over a different persona, which has its own strengths and flaws.</p>
<h3>Why do people want to role play?</h3>
<p>The fun is (for me, anyway) to play someone who is different from yourself, and to interact with others who are doing the same. Have you ever been reading a book, or watching a film, and wishing you were one of those involved and could interact with the characters? Well now you can. I don't think this is the only reason though - if you ask three different people you would probably get three different answers. Basically it all boils down to one thing: they <em>enjoy</em> it.</p>
<h3>How do I get started role playing?</h3>
<p>The first thing that you need to be able to role play is a character and a character history. What was their upbringing like? How does the character act towards other people? Does he or she dislike any particular race? You don't have to work it all out straight away, but it's good to have some ideas. Maybe your character had a poor education, and doesn't always speak correctly. For me, flaws seem to make a character come to life a bit more and make him or her more unique.</p>
<p>Think how your character will act towards other people. For example, if you play an evil character then they don't have to be polite and courteous to people they meet, but may show respect for expert thieves and murderers. That will give you something to work with.</p>
<p>You can also let the in-game events affect your character as time goes by. Say something very traumatic happens to your character. That might have an effect on their personality, like being trapped in a small place might cause them to become claustrophobic. Again, make up your own mind and use your imagination!</p>
<p>As time goes by and you develop your character, you might want to start writing their background, personality and events that have happened to him in game to remind you, and so you can read through it again later to refresh your memory as to how they might act. As an example, if they have had bad experiences with a particular type of race or creature, they may come to hate and/or fear them.</p>
<p>If you are having trouble finding role playing guilds, or even information on your chosen game then <a 
		href="http://www.stratics.com">Stratics</a> is an excellent place to start looking. They have information on message boards for many online role playing games. Since online games get updated a lot, the instruction manual is not always up to date - Stratics serves as an up to date online manual. It also reports what is happening in-game.</p>
<p>All this can only take you so far, though. To truly start role playing, you need other people to play and have adventures with. Try to find role players whereever you happen to be playing. Usually there is some sort of role playing community in an RPG (<strong>R</strong>ole <strong>P</strong>laying <strong>G</strong>ame) - look at web sites and hunt around.</p>
<h3>What language do role players speak?</h3>
<p>1337 speak (also known as leet speak, elite speak, d00d speak...), where letters are replaced by similar symbols and numbers, and acronyms like lol (laugh out loud) and rofl (rolling on the floor laughing) are generally frowned upon. Emotes such as *laughs* and *smiles* are generally preferred.</p>
<p style="text-align: right">(Reid's note: Actually, for WoW roleplaying, use the slash emotes (/smile and so on), they're much preferred. If there isn't one that fits, /e allows you to type your own emote out.)</p>
<p>Other than that, pick anything that fits with your character. Speak modern english, ye olde english, or even english with a french accent if you like. Does your character have a lisp? Do they have difficulty pronouncing 'th'? Perhaps you will have them speak Orcish, Elven or Gargish... It really doesn't matter, as long as you are consistent.</p>
<h3>Is talking normally allowed?</h3>
<p>Talking normally is normally known as going out of character, or OOC for short (this is one acronym role players do use). This seems to be a very personal thing. I personally always try to use [] to show ooc, but it's not the only way. You can just say you are talking ooc and speak normally. At some point though you will find it necessary to go ooc - don't worry, this is natural, and something we all have to do from time to time. However, some die hard role players would rather cut off their right foot than be caught talking out of character.</p>
<h3>Tolerance</h3>
<p>Just because someone does not role play in exactly the same way you do is not a reason to flame them or say they are not role playing. However, you may come across someone, either in-game or on message boards, who just seems to like arguing and attacking you personally, regardless of your arguments. These people are generally called trolls. Don't try to argue with people like this, as that is what they want - they love to argue and insult people. Just try to ignore them, and if you're on a forum you may wish to bring it to the attention of the forum staff.</p>
<h3>Role Playing for Girls</h3>
<p>Some girls do not like having guys hit on them in role-playing games. This is quite understandable. One way you can avert this from happening to you is to play a male character. Playing a different sex can be an interesting and challenging role playing experience. Of course, you don't have to play a man if you don't want to - maybe you could be that powerful girl you always looked up to.</p>
<p>Always keep in mind that just because you see a woman in-game, it does not necessarily mean they are a woman in real life. In fact, a lot of 'girls' you meet in game are in reality guys playing as a girl. I have known quite a number of girls who play rpgs, who have varied tastes. Some like to run shops, make things and sell them, others really like PvP (Player vs player combat).</p>
<p>Again, try things out and do what feels right for you. While there <em>are</em> more men playing rpgs, there are certainly lots of women too, so you will not be the only girl playing.</p>
<h3>Is role playing for me?</h3>
<p>Why are you asking me? What do you think? Want to give it a try?</p>
<p>Before moving onto the main body of the guide, I would like to thank all the people whom I have spoken to that have given me support and information which helped make this.</p>
</div>
