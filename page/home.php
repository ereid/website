<div id="column_l">
	&nbsp;<h2>Welcome!</h2>
	<p>Hello and welcome! Don't mind the occasional appearance change, I'm forever tweaking things as new, exciting web technologies come into being.</p>
	<p>Who am I? As the top of the page implies, my name's Euan. I'm a DevOps Engineer at Cloudreach, with interests ranging from archery to cooking. For more on that sort of thing, click on the <a href="/about/">about me</a> link.</p>
	<p>This site is also home to my various little coding projects that I do to pass time. If you're interested in finding out more, take a peek at the <a href="/projects/">Projects</a> page.</p>
</div>
<div id="column_r" style="padding-right: 30px; padding-top: 30px;">
	<a class="twitter-timeline" height="500" width="500" href="https://twitter.com/EuanReid" data-widget-id="498417349934845952">Tweets by @EuanReid</a>
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>
