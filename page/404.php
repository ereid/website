<div id="content">
	<h1>Oops!</h1>
	<p><strong>It seems <?php echo $_SERVER['REQUEST_URI']; ?> doesn't exist. Fear not! There's a few easy things to do here:</strong>
	<ul>
		<li>If you typed the address in yourself, double check your spelling.</li>
		<li><a href="<?php echo $_SERVER['HTTP_REFERER']?>">Go back</a> to whichever page sent you here</li>
		<li>Go to the <a href="/">home page</a>.</li>
	</ul>
</div>
