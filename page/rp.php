<div id="submenu">
	<ul>
		<li><a href="/rp/">Preface</a></li>
		<li><a href="/rp/prologue/">Prologue/FAQ</a></li>
	</ul>
</div>
<div id="column_l">
	<h2>RP for Beginners</h2>
	<p>This guide was lifted from <a href="http://www.ownedcore.com" target="_blank">OwnedCore.com</a> (formerly MMOwned.com), and was originally posted by <a href="http://www.ownedcore.com/forums/members/249848-phattwoohie.html" target="_blank">Phattwoohie</a>. The thread can be found <a href="http://www.ownedcore.com/forums/wow-guides/161445-ultimate-role-playing-guide.html" target="_blank">here</a>. I decided to upload it here as a collection of webpages suits the guide format far better than a forum post (in my opinion, at least), and also so I knew there was a permanent record of it, in case the post is deleted.</p>
	<p>I also took the liberty of changing the wording around in parts to make it, in my eyes, read better. I hope you find this guide useful. Please note, however, that as this is primarily a guide to role play in World of Warcraft, some parts may not be quite so relevant to general role playing.</p>
</div>
