<div id="column_l" style="width: 47%;">
	<h3>Me, Myself and I</h3>
	<ul>
		<li><a href="http://www.facebook.com/euan.f.reid">Facebook</a><br />
		What's to add? My profile on the monolith of social networking.</li>
		<li><a href="http://www.twitter.com/EuanReid">Twitter</a><br />
		My feed on the short-message social network that is Twitter. Latest updates from me are visible on the <a href="/">homepage</a>.</li>
		<li><a href="https://plus.google.com/100978882335199110523">Google+</a><br />
		Everyone loves Google+!</li>
		<li><a href="http://uk.linkedin.com/in/euanfreid/">LinkedIn</a><br />
		LinkedIn is a social networking site for professional networking. So Facebook, but for business.</li>
	</ul>
	<h3>Coding</h3>
	<ul>
		<li><a href="http://bitbucket.org/ereid">BitBucket</a><br/>
		Code repositories of various flavours</li>
		<li><a href="http://github.com/ReidE96">GitHub</a><br/>
		More repositories, including my CV's source .tex</li>
		<li><a href="http://www.colorzilla.com/gradient-editor/">CSS Gradient Editor</a><br/>
		A nifty tool to create multi-faceted CSS gradients</li>
	</ul>
</div>
<div id="column_r" style="padding: 0 10px; width: 47%;">
        <h3>Roleplaying</h3>
        <ul>
                <li><a href="http://www.phoenix-rp.com">Phoenix Roleplaying</a><br />
                A play-by-post roleplaying forum</li>
                <li><a href="http://www.ajjegames.com">AJJE Games</a><br />
                Another play-by-post site, with its own special posting system</li>
                <li><a href="http://www.geas.org.uk/">Grand Edinburgh Adventurer Society</a><br />
                Better known as GEAS - the University of Edinburgh RPG society</li>
                <li><a href="http://www.orcedinburgh.co.uk">ORC Edinburgh</a><br />
                Open Roleplaying Commmunity Edinburgh, for gamers who ain't students</li>
                <li><a href="http://www.black-lion.co.uk">Black Lion</a><br />
                Edinburgh's favourite friendly local game store</li>
                <li><a href="http://www.conpulsion.org">Conpulsion</a><br />
                Scotland's oldest, largest gaming convention, which I currently have the honour of organising</li>
	</ul>
</div>
