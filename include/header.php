<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $title; ?></title>
<link href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700" rel="stylesheet" type="text/css">
<link href="/css/style.css" media="screen" rel="stylesheet" title="CSS" type="text/css" />
<link rel="icon" href="/images/icon.png" type="image/x-icon" />
<link rel="shortcut icon" href="/images/icon.png" type="image/x-icon" />
<script type="text/javascript" src="/js/ga.js"></script>
<?php echo $header; ?>
</head>
